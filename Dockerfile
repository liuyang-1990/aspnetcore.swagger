# This file is a template, and might need editing before it works on your project.
FROM microsoft/dotnet:2.1-sdk
WORKDIR /build
COPY . .
RUN dotnet publish -o /build
EXPOSE 80
ENTRYPOINT ["dotnet", "ANC.Api.dll"]