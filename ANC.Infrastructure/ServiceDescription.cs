﻿using Microsoft.Extensions.DependencyInjection;
using System;


namespace ANC.Infrastructure
{
    public class ServiceDescription : ServiceDescriptor
    {

        public ServiceDescription(
            Type serviceType,
            Type implementationType,
            ServiceLifetime lifetime)
            : base(serviceType, implementationType, lifetime)
        {

        }
        public ServiceDescription(
            Type serviceType,
            object instance)
            : base(serviceType, instance)
        {

        }

        public ServiceDescription(
            Type serviceType,
            Func<IServiceProvider, object> factory,
            ServiceLifetime lifetime)
            : base(serviceType, factory, lifetime)
        {

        }


        public bool Startup
        {
            get; set;
        }
    }

}
