﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ANC.Infrastructure
{
    public static class Helper
    {
        /// <summary>
        /// Service definition collection.
        /// </summary>
        public static IServiceCollection BuildServiceDescriptionCollection()
        {
            var serviceCollection = new ServiceCollection();
            var serviceDescriptions = ScanServiceDescriptionMetas();
            foreach (var serviceDescription in serviceDescriptions)
            {
                serviceCollection.Add(serviceDescription);
            }

            return serviceCollection;
        }

        /// <summary>
        /// Scans the service description metas.
        /// </summary>
        /// <returns>The service description metas.</returns>
        private static IEnumerable<ServiceDescription> ScanServiceDescriptionMetas()
        {
            var serviceDescriptions = new List<ServiceDescription>();

            IEnumerable<Assembly> assembiles = AssemblyLoader.LoadAll();
            IEnumerable<Type> services = assembiles
                .SelectMany(assembly => AssemblyTypeLoader.GetTypes(assembly, ContainsDependencyInjectionAttribute));
            var serviceGroup = services
                .SelectMany(service => service.GetCustomAttributes<DependencyInjectionAttribute>().Select(attr => new { Attr = attr, Impl = service }))
                .GroupBy(define => define.Attr.Service);

            foreach (var service in serviceGroup)
            {
                var highPriorityImpl = service.OrderByDescending(impl => impl.Attr.Priority).First();
                serviceDescriptions.Add(new ServiceDescription(highPriorityImpl.Attr.Service, highPriorityImpl.Impl, highPriorityImpl.Attr.Lifetime));
            }

            return serviceDescriptions;
        }

        /// <summary>
        /// Containses the dependency injection attribute.
        /// </summary>
        /// <returns><c>true</c>, if dependency injection attribute was containsed, <c>false</c> otherwise.</returns>
        /// <param name="type">Type.</param>
        private static bool ContainsDependencyInjectionAttribute(Type type)
        {
            if (!type.IsClass)
            {
                return false;
            }

            if (type.IsAbstract)
            {
                return false;
            }

            return !type.GetCustomAttributes<DependencyInjectionAttribute>().IsNullOrEmpty();
        }
    }
}
