﻿using Microsoft.Extensions.DependencyInjection;
using System;


namespace ANC.Infrastructure
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class DependencyInjectionAttribute:Attribute
    {
        public DependencyInjectionAttribute(Type service)
        {
            this.Service = service;
            this.Lifetime = ServiceLifetime.Scoped;
        }

        /// <summary>
        /// Gets service type.
        /// </summary>
        public Type Service { get; private set; }

        /// <summary>
        /// Gets or sets service lifetime.
        /// </summary>
        public ServiceLifetime Lifetime { get; set; }

        /// <summary>
        /// Gets or sets priority.
        /// </summary>
        public int Priority { get; set; }

    }
}
