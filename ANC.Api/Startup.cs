﻿using ANC.Business;
using ANC.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Reflection;
namespace ANC.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private static readonly ConcurrentDictionary<string, Assembly> dicAssemblies = new ConcurrentDictionary<string, Assembly>();
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(option =>
            {
                option.CustomSchemaIds(x => x.FullName);
                option.DescribeAllEnumsAsStrings();
                option.SwaggerDoc("v1", new Info() { Title = "My API_1", Version = "V1" });
            });
            //var ss = Helper.BuildServiceDescriptionCollection();
            //var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            //foreach (var assembly in assemblies)
            //{
            //    dicAssemblies.TryAdd(assembly.FullName, assembly);
            //}

            //var assembliesDll = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).GetFiles("*.dll");

            //foreach (var file in assembliesDll)
            //{
            //    var assembly = Assembly.LoadFrom(file.FullName);
            //    dicAssemblies.TryAdd(assembly.FullName, assembly);
            //}
            //var assembiles = dicAssemblies.Values.ToArray();
            //assembiles.SelectMany(x => x.GetTypes().Where(i => !string.IsNullOrEmpty(i.GetCustomAttributes<DependencyInjectionAttribute>())))
          //  services.AddScoped<IValueBusiness, ValueBusiness>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = "swagger";
            });
        }
    }
}
