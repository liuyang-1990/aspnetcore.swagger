﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ANC.Business;
using Microsoft.AspNetCore.Mvc;

namespace ANC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IValueBusiness _valueBusiness;

        public ValuesController(IValueBusiness valueBusiness)
        {
            _valueBusiness = valueBusiness;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return   _valueBusiness.GetValue();
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
