#addin "nuget:?package=Cake.Http&version=0.5.0"
#addin "nuget:?package=Cake.Json&version=3.0.1"
#addin "nuget:?package=Newtonsoft.Json&version=9.0.1"

var scriptVersion = 0.1;

var target = Argument("target", "Default");
var branch = Argument<string>("branch", "master");

var workingDirectory = Directory(".");
var workingDirectoryFullPath = MakeAbsolute(workingDirectory).FullPath;
var environmentVariables = new Dictionary<string, string>{
                        {"CAKE_PATHS_ADDINS", workingDirectoryFullPath + "/tools/Addins"},
                        {"CAKE_PATHS_TOOLS", workingDirectoryFullPath + "/tools"},
                        {"CAKE_PATHS_MODULES", workingDirectoryFullPath + "/tools/Modules"}};
var configJsonFile = "cake-config.json";

var configuration = new Configuration();
var cakeConsole = new CakeConsole();

Setup(ctx =>
{
    var configFile = new FilePath(configJsonFile);
    configuration = DeserializeJsonFromFile<Configuration>(configFile);
    Information("Host {0}...", configuration.RemoteScriptHost);

    string versionText = HttpGet(string.Concat(configuration.RemoteScriptHost, "version.txt"));
    Information("Remote version {0};Current version {1}", versionText, scriptVersion);
    
    if (Convert.ToDouble(versionText) > scriptVersion)
    {
        Information("Please update the cake build script");
    }
});

Teardown(ctx =>
{
   Information("Finished running tasks.");
});

Task("Update-Script")
    .Does(() =>
    {
        string versionText = HttpGet(string.Concat(configuration.RemoteScriptHost, "version.txt"));
        if (Convert.ToDouble(versionText) > scriptVersion)
        {
            Information("Begin update...");
            StartProcess("git", new ProcessSettings { Arguments = "pull --rebase" });
            string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "main.cake"));

            System.IO.File.WriteAllText("build.cake", responseBody, Encoding.UTF8);

            StartProcess("git", new ProcessSettings { Arguments = "add build.cake" });
            StartProcess("git", new ProcessSettings { Arguments = string.Format("commit -m \"Updated cakebuild script version to {0}\"", versionText) });
            StartProcess("git", new ProcessSettings { Arguments = "push" });
            Information("Update success!");
        }
    });

Task("Default")
  .IsDependentOn("Clean")
  .IsDependentOn("Restore-NuGet-Packages")
  .IsDependentOn("Compile")
  .IsDependentOn("Build-UnitTests")
  .IsDependentOn("Run-UnitTests");

Task("Clean")
    .Does(() =>
    {
        Information("Current Branch is:" + branch);
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "clean.cake"));

        CakeExecuteExpression(responseBody, new CakeSettings { 
        Arguments = new Dictionary<string, string>{
                {"cleanType", configuration.CleanType},
                {"workingDirectoryPath", workingDirectoryFullPath}
            }
        });
    });

Task("Restore-NuGet-Packages")
    .Does(() =>
    {
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "restore.cake"));
        CakeExecuteExpression(responseBody, new CakeSettings { 
        Arguments = new Dictionary<string, string>{
                {"restoreType", configuration.RestoreType},
                {"workingDirectoryPath", workingDirectoryFullPath}
            }
        });
    });

Task("Compile")
    .Description("Builds all the projects in the solution")
    .Does(() =>
    {
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "build.cake"));
        CakeExecuteExpression(responseBody, new CakeSettings { 
                Arguments = new Dictionary<string, string>{
                        {"buildType", configuration.BuildType},
                        {"configuration", configuration.BuildConfiguration},
                        {"netCoreTarget", configuration.NetCoreTarget},
                        {"netStandardTarget", configuration.NetStandardTarget},
                        {"filePaths", configuration.AllProjectsPath},
                        {"workingDirectoryPath", workingDirectoryFullPath}
                    },
                EnvironmentVariables = environmentVariables
            });
    });

Task("Build-UnitTests")
    .Does(()=>{
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "build.cake"));
        CakeExecuteExpression(responseBody, new CakeSettings { 
                Arguments = new Dictionary<string, string>{
                        {"buildType", configuration.UnittestType},
                        {"configuration", configuration.BuildConfiguration},
                        {"netCoreTarget", configuration.NetCoreTarget},
                        {"netStandardTarget", configuration.NetStandardTarget},
                        {"filePaths", configuration.TestProjectsPath},
                        {"workingDirectoryPath", workingDirectoryFullPath}
                    },
                EnvironmentVariables = environmentVariables
            });
    });

Task("Run-UnitTests")
    .Description("Executes unit tests for all projects")
    .Does(() =>
    {
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "unittest.cake"));
        CakeExecuteExpression(responseBody, new CakeSettings { 
            Arguments = new Dictionary<string, string>{
                    {"UnittestType", configuration.UnittestType},
                    {"testFiles", configuration.TestFiles},
                    {"testProjectsPath", configuration.TestProjectsPath},
                    {"runsettings", configuration.TestRunsetting},
                    {"configuration", configuration.BuildConfiguration},
                    {"netCoreTarget", configuration.NetCoreTarget},
                    {"workingDirectoryPath", workingDirectoryFullPath}
                },
            EnvironmentVariables = environmentVariables
        });
    });

Task("Update-Version")
    .Does(() =>
    {
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "update_model_version.cake"));
        CakeExecuteExpression(responseBody, new CakeSettings { 
            Arguments = new Dictionary<string, string>{
                    {"updateVersionType", configuration.UpdateVersionType},
                    // {"nogit", "true"},
                    // {"isTest", "true"},
                    {"versionPrefix", configuration.VersionPrefix},
                    {"versionCorePrefix", configuration.VersionCorePrefix},
                    {"versionTageName", configuration.VersionTageName},
                    {"netCoreProjectProperties", configuration.NetCoreProjectProperties},
                    {"netCoreProject", configuration.VersionPropsPath},
                    {"workingDirectoryPath", workingDirectoryFullPath}
                },
            EnvironmentVariables = environmentVariables
        });
    });


Task("Package-NuGet")
    .IsDependentOn("Update-Version")
    .IsDependentOn("Compile")
    .Description("Generates NuGet packages for each project")
    .Does(() =>
    {
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "package_nuget.cake"));
        CakeExecuteExpression(responseBody, new CakeSettings { 
            Arguments = new Dictionary<string, string>{
                    {"packageType", configuration.PackageType},
                    {"netProject", configuration.NetProject},
                    {"netCoreProject", configuration.NetCoreProject},
                    {"configuration", configuration.BuildConfiguration},
                    {"workingDirectoryPath", workingDirectoryFullPath}
                }
        });
    });

Task("Publish-NuGet")
    .IsDependentOn("Package-NuGet")
    .Description("Pushes the nuget packages in the nuget folder to a NuGet source. Also publishes the packages into the feeds.")
    .Does(() =>
    {
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "publish_nuget.cake"));
        CakeExecuteExpression(responseBody, new CakeSettings { 
            Arguments = new Dictionary<string, string>{
                    {"apiKey", configuration.ApiKey},
                    {"workingDirectoryPath", workingDirectoryFullPath}
                }
        });
    });

Task("Clean-branch")
    .Description("Clean up the git branch")
    .Does(() =>
    {
        string responseBody = HttpGet(string.Concat(configuration.RemoteScriptHost, "clean_git_branch.cake"));
        CakeExecuteExpression(responseBody, new CakeSettings { 
            Arguments = new Dictionary<string, string>{
                    {"userName", "admin"},
                    {"password", "admin"},
                    {"workingDirectoryPath", workingDirectoryFullPath}
                },
            EnvironmentVariables = environmentVariables
        });
    });

RunTarget(target);



/*===============================================
================= HELPER METHODS ================
===============================================*/

public class Configuration
{
    public string RemoteScriptHost {get;set;}
    public string CleanType {get;set;}
    public string BuildType {get;set;}
    public string RestoreType {get;set;}
    public string UnittestType {get;set;}
    public string UpdateVersionType {get;set;}
    public string PackageType {get;set;}
    public string AllProjectsPath {get;set;}
    public string TestProjectsPath {get;set;}
    public string TestFiles {get;set;}
    public string TestRunsetting {get;set;}
    public string VersionPrefix {get;set;}
    public string VersionCorePrefix {get;set;}
    public string VersionTageName {get;set;}
    public string ApiKey {get;set;}
    public string BuildConfiguration {get;set;}
    public string NetStandardTarget {get;set;}
    public string NetCoreTarget {get;set;}
    public string NetProject {get;set;}
    public string NetCoreProjectProperties {get;set;}
    public string NetCoreProject {get;set;}
    public string VersionPropsPath {get;set;}
}


public void WriteError(string errorMessage)
{
    cakeConsole.ForegroundColor = ConsoleColor.Red;
    cakeConsole.WriteError(errorMessage);
    cakeConsole.ResetColor();
}
