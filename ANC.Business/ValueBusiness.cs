﻿using ANC.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace ANC.Business
{
    [DependencyInjection(typeof(IValueBusiness))]
    public class ValueBusiness : IValueBusiness
    {
        public string GetValue()
        {
            return "Test";
        }
    }
}
