a aspnet core web api project.

.gitlab_ci.yml 



var path = require('path');
var sourcePath = path.resolve(__dirname, './src');
var HtmlWebpackPlugin = require("html-webpack-plugin");
const tsImportPluginFactory = require('ts-import-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
    mode: "development",
    entry: {
        index: "./src/index.tsx"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].js"
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            getCustomTransformers: () => ({
                                before: [
                                    tsImportPluginFactory(
                                        {
                                            libraryName: "antd",
                                            libraryDirectory: 'es',
                                            style: "css"
                                        }
                                    )
                                ]
                            }),
                            // compilerOptions: {
                            //     module: 'es2015'
                            //   }
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: devMode ? "style-loader" : MiniCssExtractPlugin.loader,
                        // options: {
                        //     publicPath: "/dist"
                        // },
                    },
                    "css-loader"
                ]
                // use: [
                //     { loader: "style-loader" },
                //     { loader: "css-loader" }
                // ]
            },
            {
                test: /\.(png|jpg|gif|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192
                        }
                    }
                ]
            }
        ]


    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'public/index.html',
        }),
        new MiniCssExtractPlugin({
            filename: "style.css",
        })
    ],
    // devServer: {
    //     contentBase: sourcePath,
    //     hot: true,
    //     stats: {
    //         warnings: false
    //     },
    // },

}













































import { Button, Form, Input, Select } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import * as React from 'react';
import Editor from './Editor';
const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;
export interface IArticle extends FormComponentProps {
  title: string
  abstract: string
  tag: string
  type: string
  content: string
  nature: string
  addArticle: (payload: object) => void
}
class AddArticle extends React.Component<IArticle,any> {

  constructor(props: any) {
    super(props);
    this.state = {
      editorState: null
    }
  }


  public handleSubmit = (e: any) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let html = this.state.editorState.toHTML();
        console.log(html);
        //this.props.addArticle(values)
      }
    })
  }

  public onChange = (content: any) => {
    this.setState({
      editorState: content
    });
  }
  public render() {
    const { getFieldDecorator } = this.props.form
    const selectBefore = getFieldDecorator('nature', { initialValue: '原创' })(
      <Select style={{ width: 70 }}>
        <Option value="原创">原创</Option>
        <Option value="转载">转载</Option>
      </Select>
    )
    // const forItemlayout= { labelCol: {span:1}, wrapperCol: {span: 20}};
    const forItemlayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 1 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 22 },
      },
    }
    return (


      <div>
        <Form onSubmit={this.handleSubmit}>
          <FormItem hasFeedback={true} label="标题" {...forItemlayout} >
            {getFieldDecorator('title', {
              rules: [{ required: true, message: '请填写文章标题!' }]
            })(
              <Input
                addonBefore={selectBefore}
                placeholder="请填写文章标题"
              // style={{ width: 220 }}
              />
            )}
          </FormItem>
          <FormItem label="内容" {...forItemlayout}>
            {getFieldDecorator('content', {
              rules: [{ required: true, message: '文章摘要!' }]
            })(<Editor onChange={this.onChange} />)}
          </FormItem>
          <FormItem hasFeedback={true} label="Tag标签"  {...forItemlayout}>
            {getFieldDecorator('tag', {
              rules: [{ required: true, message: '文章标签!' }]
            })(<Input placeholder="填写个文章标签吧" style={{ width: 200 }} />)}
          </FormItem>
          <FormItem hasFeedback={true} label="文章类型"  {...forItemlayout}>
            {getFieldDecorator('type', {
              initialValue: '',
              rules: [{ required: true, message: '文章类型!' }]
            })(
              <Select style={{ width: 200 }}>
                <Option value="">请选择类型</Option>
                <Option value="typescript">typescript</Option>
                <Option value="javascript">javascript</Option>
                <Option value="react">react</Option>
                <Option value="node">node</Option>
                <Option value="css">css</Option>
              </Select>
            )}
          </FormItem>
          <FormItem hasFeedback={true} label="摘要"  {...forItemlayout}>
            {getFieldDecorator('abstract', {
              rules: [{ required: true, message: '文章摘要!' }]
            })(<TextArea placeholder="文章摘要!" autosize={{ minRows: 2, maxRows: 6 }} />)}
          </FormItem>
          <FormItem wrapperCol={{
            xs: { span: 24, offset: 0 },
            sm: { span: 23, offset: 1 },
          }}>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button">
              发表文章
            </Button>
          </FormItem>

        </Form>
      </div>
    )
  }
}

export default Form.create()(AddArticle)

























{
  "name": "blog",
  "version": "0.1.0",
  "private": true,
  "dependencies": {
    "@types/webpack": "^4.4.19",
    "antd": "^3.10.8",
    "braft-editor": "^2.1.33",
    "css-loader": "^1.0.1",
    "html-webpack-plugin": "^3.2.0",
    "mini-css-extract-plugin": "^0.4.5",
    "react": "^16.6.3",
    "react-dom": "^16.6.3",
    "style-loader": "^0.23.1",
    "ts-loader": "^5.3.0",
    "ts-node": "^7.0.1",
    "webpack": "^4.26.0",
    "webpack-cli": "^3.1.2",
    "webpack-dev-server": "^3.1.10"
  },
  "scripts": {
    "start": "webpack-dev-server -d --history-api-fallback --hot --inline --progress --colors --port 3000 --open",
    "build": "webpack -p --progress --colors"
  },
  "devDependencies": {
    "@types/jest": "^23.3.9",
    "@types/node": "^10.12.9",
    "@types/react": "^16.7.6",
    "@types/react-dom": "^16.0.9",
    "ts-import-plugin": "^1.5.5",
    "typescript": "^3.1.6"
  }
}
